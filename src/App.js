import React from 'react'
import Content from './components/Content'

import IntroSection from './components/IntroSection'
import NavBar from './components/NavBar'

import SideBar from './components/SideBar'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import './App.css'
import './components/FullWidthTabs';
// import ActivityCard from '.components/ActivityCard';





import Grid from '@mui/material/Grid'


export default function App() {
  return (
    <React.Fragment>

   
    <div className="navbar-demo d-flex">
      <div >
        <SideBar />
      </div>
      <div>
        <Grid container>
          <Grid></Grid>
          <Grid>
            <NavBar />
            <IntroSection />
            <Content />
          
          </Grid>
        </Grid>
      </div>
      

      
    </div>

   
    </React.Fragment>
  )
}
