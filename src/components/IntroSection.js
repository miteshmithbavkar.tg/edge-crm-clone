import React from 'react'
import './assets/IntroSection.css'
import WestIcon from '@mui/icons-material/West';

export default function IntroSection() {
  return (
    <div className="Introsection">
      <div className="card card-introsection">
        <div className="Leftintro-section">
          <div className="icon-intro-section">
            
            <span className='intro-wrap'><span className='intro-icon'><WestIcon style={{fontSize:'15px'}}/></span>Repository List</span>
          </div>
          <div className="link-intro-section">
            <ul className="link-intro">
              <li className='intro-wrap' ><a>Create Lead</a></li>
              <li className='intro-wrap'><a>Next</a></li>
            </ul>
          </div>
        </div>
        <div className="Rightintro-section">
        <h3 className='Rightintro-section-heading'>Peter Petrino</h3> 
         <p className='Rightintro-section-subheading'>Bigham Jewelers</p>
        </div>
      </div>
      <br />
    </div>
  )
}
