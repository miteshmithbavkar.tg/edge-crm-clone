import React from 'react'
import './assets/FormSection.css'
import ArrowRightAltOutlinedIcon from '@mui/icons-material/ArrowRightAltOutlined'

import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import { TextField } from '@mui/material'

export default function FormSection() {
  const [value, setValue] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div className="form section">
      <div>
        <div class="card card-form">
          <div class="card-body">
            <Box sx={{ width: '100%', typography: 'body1' }}>
              <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  <TabList
                    onChange={handleChange}
                    aria-label="lab API tabs example"
                    className='formtab'
                  >
                    <Tab  className='formlabel' label="Next Value" value="1" />< ArrowRightAltOutlinedIcon className='my-3'/>
                    <Tab className='formlabel' label="Update" value="2" />
                    <Tab className='formlabel' label="Meeting" value="3" />
                    <Tab className='formlabel' label="Call" value="4" />
                    <Tab className='formlabel' label="Task" value="5" />
                    <Tab className='formlabel' label="Email" value="6" />
                  </TabList>
                </Box>
                <TabPanel value="1"></TabPanel>
                <TabPanel value="2"><textarea placeholder='Type Your Update Here' ></textarea></TabPanel>
                <TabPanel value="3">Item Three</TabPanel>
                <TabPanel value="4">Item Three</TabPanel>
                <TabPanel value="5">Item Three</TabPanel>
                <TabPanel value="6">Item Three</TabPanel>
              </TabContext>
             
            </Box>

          </div>
        </div>
      </div>
    </div>
  )
}
