import React from 'react'
import logoImage from './logo.jpg'
import PhoneInTalkOutlinedIcon from '@mui/icons-material/PhoneInTalkOutlined'
import './Activity.css'
import './assets/NavSection.css'

export default function CardActivity2() {
    const peoples=[
        {
          'task': 'Created a call',
          'description': 'Call to check RESULT'
        },
        {
          task: 'Created a task ',
          description: 'hdhhh hj h hj hj hj hj hkhjkhk jhkhkhkh hkhkhkhk kjh kh kh kh hjk hkh khkhk '
        },
        {
          'task': ' Created a call',
          'description': 'JJASDJLAJSLDALSJL JLJL JL JLJL JL L JL J'
        },
        {
          'task': 'Created a call  ',
          'description': 'UDUSAOUOUOUO UOU OUO UO U OUO UO UO'
        },
        {
          'task': 'Updated a meeting ',
          'description': 'adhgjgjgcgj gjg jg jg j g j gj gj gjg jg j'
        },
     ];
  return (

         peoples.map(person=>{
  
    return(
             
                     
      <div className="navcard my-2">

        <PhoneInTalkOutlinedIcon className="activitycaricon" />

        <div>
        
          <div class="card activity-resize my-3">
            <div class="card-body navcard-body">
              <div className="activity-rightsection">
                <a>
                  <img src={logoImage} className="image-navresize"></img>
                </a>
              </div>
              <div className="">
                <p className="heading-activity">
                  <strong>Edge Consultant</strong>
                </p>
                <p className="activity-subtitile ">
                {person.task}
                  <span className="activity-subtitle">
                   '{person.description }'
                  </span>
                  dated Apr 22, 2022 .
                </p>
              </div>
            </div>
          </div>
      <div className="actvity-button">
        <button className="button-hover">View</button>
        <button className="button-hover">Edit</button>
        <button className="text-danger button-dangerhover">Delete</button>
      </div>
        </div>
      </div>
                 ) 
             })

  )
}
