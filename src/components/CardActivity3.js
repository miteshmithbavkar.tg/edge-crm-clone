import React from 'react'
import './assets/NavSection.css'

import './Activity.css'
import logoImage1 from './logo.jpg'
import PhoneInTalkOutlinedIcon from '@mui/icons-material/PhoneInTalkOutlined'



export default function CardActivity3() {
  const peoples=[
    {
      'task': 'Created a call',
      'description': 'Call to check RESULT',
      'date':'dated Apr 22, 2022 .'

    },
    {
      'task': 'Created a call ',
      'description': 'JJASDJLAJSLDALSJL JLJL JL JLJL JL L JL J',
      'date':'dated Apr 21, 2022 .'
      
    },
    {
      'task': ' Created a call',
      'description': 'UDUSAOUOUOUO UOU OUO UO U OUO UO UO',
      'date':'dated Apr 21, 2022 .'
    },
    {
      'task': 'Updated a call  ',
      'description': 'hkhkhkhk khhjhjhjh kkhkhkhkh',
      'date':' dated Apr 12, 2022 .'
    },
 ,
 ];
  return (
    peoples.map(person=>{
            
      console.log({person})
           return(
            <div className="navcard my-3">
              <PhoneInTalkOutlinedIcon className="activitycaricon" />

        
            <div>
              
              <div class="card navcard-resize">
              
                <div class="card-body navcard-body">
                  <div className="navcard-leftsection">
                    <a>
                      <img
                        src={logoImage1}
                        className="image-navresize"
                      ></img>
                    </a>
                  </div>
                  <div className="navcard-rightsection">
                    <p className="heading-nav">
                      Edge Consultant <span></span>
                    </p>
                    <p className="nav-subtitile">
                    {person.task}<span className='' style={{color:'orange'}}>'{person.description}'</span>{person.date}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

               
           ) 
       })   
  )
}
