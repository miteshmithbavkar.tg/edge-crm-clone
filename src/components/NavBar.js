import React from 'react'
import './assets/Navbar.css'
import { AppBar } from '@mui/material'

import Toolbar from '@mui/material/Toolbar'

import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined'
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined'
import NotificationsOutlinedIcon from '@mui/icons-material/NotificationsOutlined'
import SearchIcon from '@mui/icons-material/Search';

import AppsOutlinedIcon from '@mui/icons-material/AppsOutlined'
export default function NavBar() {
  return (
    
      <div position="fixed" className='appbar'>
        <Toolbar>
          <div className="Navbar-section">
            <div className="Navbar-Leftsection">
              <div className="Heading-section">
                <span className='heading'>edge</span> <span className="subb-heading ">Simple,yet Powerful</span>
              </div>
            </div>
            <div className="Navbar-Rightsection">
              <div className="Icon-Navbar">
                <span>
                  <SearchOutlinedIcon  />
                </span>
                <span>
                  <AddCircleOutlineOutlinedIcon />
                </span>
                <span >
                  <NotificationsOutlinedIcon />
                </span>
                <span>
                  <AppsOutlinedIcon />
                </span>
              </div>

              <div className="Navbar-content">Hi,  Edge Consultant</div>
            </div>
          </div>
        </Toolbar>
      </div>
    
  )
}
