import React from 'react'
import './assets/Content.css'
import FormSection from './FormSection'
import NavtabSection from './NavtabSection'
import ArrowForwardIosOutlinedIcon from '@mui/icons-material/ArrowForwardIosOutlined'
import KeyboardArrowDownSharpIcon from '@mui/icons-material/KeyboardArrowDownSharp'

export default function Content() {
  return (
    <div className="wrapper">
      <div className="cards-section col-4">
        <div class="card ">
          <div class="card-body  card-display">
            <p className='firstContact'> Contact information</p>
            <a>
              <KeyboardArrowDownSharpIcon style={{fontSize:'40px',  }}/>
            </a>
          </div>

          <div className="card-size">
            <label>Contact Number 1</label> <br/>
            <input className="" placeholder="Phone No" />
          </div>
          <div className="card-size">
            <label>Contact Number 2</label><br/>
            <input class="" placeholder="Phone No" />
          </div>
          <div className="card-size">
            <label>Email Id</label><br/>
            <input
              class=""
              placeholder="nileshmhas@gmail.com "
            /><br /><br/>
          </div>
        </div>
        <br />
        <div>
          <div class="card  ">
            <div class="card-body card-display">
              <p className="">About Peter Petrino</p>
              <a>
                <ArrowForwardIosOutlinedIcon />
              </a>
            </div>
          </div>
          <br />
          <div class="card">
            <div class="card-body card-display">
              <p>Contact Address</p>
              <a>
                <ArrowForwardIosOutlinedIcon />
              </a>
            </div>
          </div>
          <br />
          <div class="card">
            <div class="card-body card-display">
              <p> Peter Petrino Leads </p>
              <a>
                <ArrowForwardIosOutlinedIcon />
              </a>
            </div>
          </div>
          <br />
          <div class="card">
            <div class="card-body card-display">
              <p>Files </p>
              <a>
                <ArrowForwardIosOutlinedIcon />
              </a>
            </div>
          </div>
          <br />
        </div>
      </div>
      <div className="form-section ">
        <FormSection />
        <NavtabSection />
        <div className="nav-section"></div>
      </div>
    </div>
  )
}
