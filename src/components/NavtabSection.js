import React from 'react'
import './assets/NavSection.css'
// import  React from 'react';
import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'

import campaign from './CampignImg.png'
import Activity from './Activity'
import logoImage from './logo.jpg';
import CardactivitySection from './CardactivitySection'
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined'
export default function NavtabSection() {
  const [value, setValue] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
    
  }
 

  
  return (
    
    <div className="navtabsection">
     
      <div>
        <Box sx={{ width: '100%', typography: 'body1' }}>
          <TabContext value={value} className="mx-3">
            <Box
              sx={{ borderBottom: 1, borderColor: 'divider' }}
              className="boxnav"
            >
              <TabList
                className="navtab"
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                <Tab className="navtab" label="Activity" value="1" />
                <Tab className="navtab" label="Campaign Emailers" value="2" />
                <Tab className="navtab" label="Logs" value="3" />
              </TabList>
            </Box>

            <TabPanel value="1">
              <Activity />
            </TabPanel>
            <TabPanel value="2">
              <div className="navcamimage">
                <img src={campaign}></img>
              </div>
            </TabPanel>
            <TabPanel value="3">
              <div className="navcard">
                <AssignmentOutlinedIcon className="navcaricon" />

                <div>
                  <div class="card navcard-resize">
                    <div class="card-body navcard-body">
                      <div className="navcard-leftsection">
                        <a>
                          <p className="LogoNav-resize">AWI</p>
                        </a>
                      </div>
                      <div className="navcard-rightsection">
                        <p className="heading-nav">
                          A Wanda Maximoff<span>02-May-2022 12:53</span>
                        </p>
                        <p className="nav-subtitile">
                          A Wanda Maximoff has updated the call : test
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div>

                <CardactivitySection/>
              </div>

              <div className="text-center navsection-link my-">
                <a>Show More</a>
              </div>
            </TabPanel>
          </TabContext>
        </Box>
      </div>
    </div>
  )
}
