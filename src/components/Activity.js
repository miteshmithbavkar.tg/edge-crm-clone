import React from 'react'
import Box from '@mui/material/Box'
import Tab from '@mui/material/Tab'
import TabContext from '@mui/lab/TabContext'
import TabList from '@mui/lab/TabList'
import TabPanel from '@mui/lab/TabPanel'
import './Activity.css'
import './assets/NavSection.css'
import CardActivity2 from './CardActivity2'

import contentimag from './edgecrmcontent.png'
import logoImage from './logo.jpg'
import PhoneInTalkOutlinedIcon from '@mui/icons-material/PhoneInTalkOutlined'
import CardActivity3 from './CardActivity3'
import './Activity.css'

export default function Activity() {
  const [value, setValue] = React.useState('1')

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }


  return (
       
    <div>
      <Box sx={{ width: '100%', typography: 'body1' }}>
        <TabContext value={ value}>
          <Box>
            <TabList
              className="activity-label"
              onChange={handleChange}
              aria-label="lab API tabs example"
            >
              <Tab className="demo demofirst" label="All" value="1" />
              <Tab className="demo" label="Meeting" value="2" />
              <Tab className="demo" label="Call" value="3" />
              <Tab className="demo" label="Task" value="4" />
              <Tab className="demo" label="Update" value="5" />
              <Tab className="demo" label="Email" value="6" />
            </TabList>
          </Box>
          <TabPanel value="1">
          <div className="activity-mainheading my-3">
              <p className='para '>Past</p>
            </div>
            
              <CardActivity2/>

            
          </TabPanel>
          <TabPanel value="2">
            <div className="activity-mainheading my-3">
              <p>Past</p>
            </div>

            <div className="navcard my-2">
              <PhoneInTalkOutlinedIcon className="activitycaricon" />

              <div>
                <div class="card activity-resize my-3">
                  <div class="card-body navcard-body">
                    <div className="activity-rightsection">
                      <a>
                        <img src={logoImage} className="image-navresize"></img>
                      </a>
                    </div>
                    <div className="">
                      <p className="heading-activity">
                        <strong>Edge Consultant</strong>
                      </p>
                      <p className="activity-subtitile">
                        Updated a meeting{' '}
                        <span className="activity-subtitle">
                          {' '}
                          'adhgjgjgcgj gjg jg jg j g j gj gj gjg jg j'
                        </span>{' '}
                        dated Apr 21, 2022 .
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="actvity-button">
              <button className="button-hover">View</button>
              <button className="button-hover">Edit</button>
              <button className="text-danger button-dangerhover">Delete</button>
            </div>
          </TabPanel>
          <TabPanel value="3">
          <div className="activity-mainheading my-3">
              <p >Past</p>
            </div>
              <CardActivity3/>

          </TabPanel>
          <TabPanel value="4">
     
            <div className="activity-mainheading my-3">
              <p>Past</p>
            </div>
            <div className="navcard my-2">
              <PhoneInTalkOutlinedIcon className="activitycaricon" />

              <div>
                <div class="card activity-resize my-3">
                  <div class="card-body navcard-body">
                    <div className="activity-rightsection">
                      <a>
                        <img src={logoImage} className="image-navresize"></img>
                      </a>
                    </div>
                    <div className="">
                      <p className="heading-activity">
                        <strong>Edge Consultant</strong>
                      </p>
                      <p className="activity-subtitile">
                        Created a task{' '}
                        <span className="activity-subtitle">
                          {' '}
                          ''hdhhh hj h hj hj hj hj hkhjkhk jhkhkhkh hkhkhkhk kjh
                          kh kh kh hjk hkh khkhk'
                        </span>{' '}
                        dated Apr 21, 2022 .
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="actvity-button">
              <button className="button-hover">View</button>
              <button className="button-hover">Edit</button>
              <button className="text-danger button-dangerhover">Delete</button>
              <button className="button-hover">Mark as Complete</button>
            </div>
          </TabPanel>
          <TabPanel value="5">
            <div className="navimage">
              <img src={contentimag}></img>
            </div>
          </TabPanel>
          <TabPanel value="6">
            <div className="navimage">
              <img src={contentimag}></img>
            </div>
          </TabPanel>
        </TabContext>
      </Box>
    </div>
  )
}
