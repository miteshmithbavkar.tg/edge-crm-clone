import React from 'react'
import DashboardOutlinedIcon from '@mui/icons-material/DashboardOutlined'
import './assets/SideBar.css'
// import imglogo from './logo.jpg'
import DateRangeIcon from '@mui/icons-material/DateRange'
import CardTravelIcon from '@mui/icons-material/CardTravel'
import WbIncandescentOutlinedIcon from '@mui/icons-material/WbIncandescentOutlined'
import LocalLibraryOutlinedIcon from '@mui/icons-material/LocalLibraryOutlined'
import CampaignOutlinedIcon from '@mui/icons-material/CampaignOutlined'
import SupportAgentOutlinedIcon from '@mui/icons-material/SupportAgentOutlined'
import GavelOutlinedIcon from '@mui/icons-material/GavelOutlined'
import AutoGraphOutlinedIcon from '@mui/icons-material/AutoGraphOutlined'
import logoImage from './logo.jpg'

import PermContactCalendarOutlinedIcon from '@mui/icons-material/PermContactCalendarOutlined'

export default function SideBar() {
  return (
    <div className="Left-SideBar">
      
      <ul>
        <li>
          <a><img src={logoImage} className='image-resize'></img></a>
        </li>
        <li>
          <a>
            <span>
              <DashboardOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <DateRangeIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <CardTravelIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <PermContactCalendarOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <WbIncandescentOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span className='color-icon'>  
              <LocalLibraryOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <CampaignOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <SupportAgentOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <GavelOutlinedIcon />
            </span>
          </a>
        </li>
        <li>
          <a>
            <span>
              <AutoGraphOutlinedIcon />
            </span>
          </a>
        </li>
      </ul>
    </div>
  )
}
